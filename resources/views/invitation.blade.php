<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Invitation</title>
	<style type="text/css">
		.outer{
			font-family: arial;
			text-transform: uppercase;
			border: 4px solid #000;
			margin-left:0cm;
			margin-right: 0.3cm;
			height:12cm;
			width:8cm;
			border-radius: 0cm;
			margin-bottom: 0.5cm;
			margin-top: 0cm;
			
		}
		.container {
		    position: relative;
		    text-align: center;
		    color: #ffffff;
		}
		/* Top left text */
		.top-left {
		    position: absolute;
		    top: 30px;
		    left: 50px;
		    width: 50%;
		    text-align: center;
		    font-size: 18px;
		}
		.qr-barcode{
			position: absolute;
		    top: 163px;
		    left: 100px;
		}
		.barcode{
			position: absolute;
		    top: 8px;
		    right: -15px;
		}
	</style>
	<script type="text/javascript" src="barcode.js"></script>
</head>
<body>
    <div class="outer">
		<div class="container">
		  	<img src="{{ asset('images/tif_inv.png') }}" width="302px" height="455px">

			<div style="transform: rotate(-90deg); border: 0px solid #00F; overflow: hidden; width: 3.5cm; height: 2cm; margin-left: 190px; margin-top: -290px;">	
				<text style="font:24px monospace;color:black" text-anchor="middle" x="75" y="62">{{ $lottery_number }}</text>
			</div>
		</div>
	</div>
</body>
</html>