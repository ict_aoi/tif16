<?php namespace App\Http\Controllers;

use DB;
use PDF;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Employee;
use App\Models\Temporary;
use App\Models\Participant;

class PartisipanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except([
            'index',
        ]);
    }
    public function index()
    {
        return view('admin.partisipans');
    }
    public function uploadFormPartisipan(Request $request)
    {  
        $array              = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);
            $path = $request->file('upload_file')->getRealPath();
           
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();
            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    foreach ($data as $key => $value) 
                    {
                        $nik          = trim($value->nik);
                        $nama         = $value->nama;
                        $dataEmployee = DB::connection('data-warehouse')
                                    ->table('employees')
                                    ->select(
                                        'empid','name','unit_name','position_name','area',db::raw("to_char(birth_date, 'ddmmyyyy')as password")
                                    )
                                    ->where('termination','false')
                                    ->where('empid',$nik)
                                    ->first();

                        if (!$dataEmployee) {
                            $dataEmployee = DB::connection('attendance_aoi')
                                    ->table('adt_new_employee')
                                    ->select(
                                        db::raw('nik as empid'),'name',db::raw('department_name as unit_name'),db::raw('position as position_name'),db::raw('factory as area'),'password'
                                    )
                                    ->where('status','aktif')
                                    ->where('nik',$nik)
                                    ->first();
                        }
                        if (!$dataEmployee) {
                            Temporary::create([
                                'column_1'       => $nik,
                                'column_2'       => $nama,
                                'column_3'       => 'Nik tidak di temukan',
                            ]);

                            $obj             = new stdClass();
                            $obj->nik        = $nik;
                            $obj->nama       = $nama;
                            $obj->is_error   = true;
                            $obj->system_log = 'Nik tidak di temukan';
                            
                            $array []             = $obj;
                        }else{
                            $checkParticipant = Participant::where('nik',$nik)->first();
                            if ($checkParticipant) {
                                Temporary::create([
                                    'column_1'       => $nik,
                                    'column_2'       => $nama,
                                    'column_3'       => 'Nik sudah ada',
                                ]);

                                $obj             = new stdClass();
                                $obj->nik        = $nik;
                                $obj->nama       = $nama;
                                $obj->is_error   = true;
                                $obj->system_log = 'Nik sudah ada';

                                $array []             = $obj;
                            } else {
                                $lottery_number= self::random();
                                Participant::create([
                                    'nik'            => $nik,
                                    'name'           => $dataEmployee->name,
                                    'unit_name'      => $dataEmployee->unit_name,
                                    'position_name'  => $dataEmployee->position_name,
                                    'factory'        => $dataEmployee->area,
                                    'password'       => $dataEmployee->password,
                                    'lottery_number' => $lottery_number,
                                    'is_invitation'  => true,
                                ]);
                                $file           = Config::get('storage.invitation');
                                
                                PDF::loadView('invitation',compact('lottery_number'))->save($file.'/BIF17_'.$dataEmployee->empid.'.pdf');
                                $obj             = new stdClass();
                                $obj->nik        = $nik;
                                $obj->nama       = $nama;
                                $obj->is_error   = false;
                                $obj->system_log = 'Berhasil';

                                $array []             = $obj;
                            }
                        }

                    }  


                    DB::commit();   
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json($array,200);
            }else
            {
                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
            }


        }
    }
    
    static function random()
    {
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

                
            $referral_code  = Carbon::now()->format('u');
            $lottery_number = $referral_code+Participant::where('lottery_number',$referral_code)->count() + 1;
            $try -= 1;
            
        } while (Participant::where('lottery_number',$lottery_number)->exists());

        return $lottery_number;
    }
    static function insertEmployee()
    {
        $dataAoi = DB::connection('attendance_aoi')
                        ->table('adt_new_employee')
                        ->select(
                            db::raw('nik as empid'),'name',db::raw('department_name as unit_name'),db::raw('position as position_name'),db::raw('factory as area'),'password'
                        )
                        ->where('status','aktif')
                        ->get();
        $dataBbip = DB::connection('attendance_bbip')
                        ->table('v_temp_bbiuser')
                        ->select(
                            db::raw('nik as empid'),'name',db::raw('department_name as unit_name'),db::raw('position as position_name'),db::raw('factory as area'),'password'
                        )
                        ->get();
        foreach ($dataAoi as $key => $emp) {
            Employee::create([
                'empid'         => $emp->empid,
                'name'          => strtolower($emp->name),
                'unit_name'     => strtolower($emp->unit_name),
                'position_name' => strtolower($emp->position_name),
                'factory'       => strtolower($emp->area),
                'password'      => strtolower($emp->password)
            ]);
        }
        foreach ($dataBbip as $key => $emp2) {
            Employee::create([
                'empid'         => $emp2->empid,
                'name'          => strtolower($emp->name),
                'unit_name'     => strtolower($emp->unit_name),
                'position_name' => strtolower($emp->position_name),
                'factory'       => strtolower($emp->area),
                'password'      => strtolower($emp->password)
            ]);
        }
    }
    static function updatePassword()
    {
        $data = Employee::where('password','')->get();
        
        foreach ($data as $key => $dt) {
            $dtPassword = DB::connection('data-warehouse')
                ->table('employees')
                ->select(
                    'empid','name','unit_name','position_name','area',db::raw("to_char(birth_date, 'ddmmyyyy')as password")
                )
                ->where('termination','false')
                ->where('empid',$dt->empid)
                ->first();
            if ($dtPassword) {
                DB::table('employees')
                ->where('empid',$dt->empid)
                ->update([
                    'password' => $dtPassword->password
                ]);
            }
            
        }
    }
}