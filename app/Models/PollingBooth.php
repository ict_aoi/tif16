<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PollingBooth extends Model
{
    protected $fillable = [
        'voter_id',
        'participant_id',
    ];

    public function voter()
    {
        return $this->belongsTo('App\Models\Voter','voter_id');
    }

}