<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryTheme extends Model
{
    public $timestamps      = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['category'
        ,'subco'
        ,'theme'
    ];

    public function getThemeAttribute($value)
	{
	    return ucwords($value);
    }
}
