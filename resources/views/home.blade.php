@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Vote IG</div>

                <div class="card-body">
                    @foreach ($resultIgs as $igVoter)
                        <div class="col my-1">
                            <div class="card card-ig" id="idIG{{ $igVoter->voter_id }}">
                                <div class="embed-responsive embed-responsive-4by3">
                                <video  controls disablepictureinpicture controlsList="nodownload noremoteplayback noplaybackrate" class="embed-responsive-item video-full">
                                    <source src="{{ $igVoter->voter->voter_url }}" type="video/mp4" />
                                </video>
                            </div>
                                <div class="card-body">
                                    <i class="fas fa-heart mr-2" style="color:red"></i> <span class="total-vote" id="totalIg{{ $igVoter->voter_id }}">{{ isset($igVoter->total) ? $igVoter->total : 0 }}</span>
                                </div>
                            </div> 
                        </div> 
                  @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Vote Tiktok</div>

                <div class="card-body">
                    @foreach ($resultTiktos as $resultTikto)
                    <div class="col my-1">
                        <div class="card card-ig" id="idIG{{ $resultTikto->voter_id }}">
                            <div class="embed-responsive embed-responsive-4by3">
                            <video  controls disablepictureinpicture controlsList="nodownload noremoteplayback noplaybackrate" class="embed-responsive-item video-full">
                                <source src="{{ $resultTikto->voter-> voter_url }}" type="video/mp4" />
                            </video>
                        </div>
                            <div class="card-body">
                                <i class="fas fa-heart mr-2" style="color:red"></i> <span class="total-vote" id="totalIg{{ $resultTikto->voter_id }}">{{ isset($resultTikto->total) ? $resultTikto->total : 0 }}</span>
                            </div>
                        </div> 
                    </div> 
                  @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
