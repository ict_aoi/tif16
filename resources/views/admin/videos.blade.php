@extends('admin.template')

@section('content')

    <!-- Main Content -->
    <div id="content">

        @include('admin.top_bar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Lucky Spin</h1>
            </div>

            <!-- Content Row -->
            <div class="row">

                <div class="col-lg-12 mb-12">

                    <div class="card shadow mb-4">
                        <div class="card-body">

                        <!-- Spinner -->
                        <form method="POST" action="{{ route('videos.uploadedVideo') }}" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            <div>
                                <label>Title</label>
                                <input type="text" name="title" placeholder="Enter Title">
                            </div>
                            <div >
                                <label>Choose Photo/Video</label>
                                <input type="file"  name="video">
                            </div>
                            <hr>
                                <button type="submit" >Submit</button>
                        </form>
                        <!-- End Spinner -->

                        <br>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

@endsection

@section('js')

@endsection
