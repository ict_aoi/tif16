<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use Carbon\Carbon;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            'name'                  => 'system',
            'email'                 => 'system@bbi-apparel.com',
            'email_verified_at'     => carbon::now(),
            'password'              => bcrypt('password1'),
        ]);
    }
}
