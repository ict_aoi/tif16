<?php

use Carbon\Carbon;
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
	protected $fillable  = ['nik','name','unit_name','position_name','factory','password','email','phone_number','registration_at','voted_at','lottery_number','is_invitation','is_exclude','is_get_doorprize'];
	protected $dates     = ['created_at'];

    
}