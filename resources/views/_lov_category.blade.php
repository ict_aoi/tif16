<table class="table table-striped">
	<thead>
	  <tr>
		<th>No</th>
		<th>Subco</th>
		<th>Theme</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $list->subco }}</td>
				<td>{{ $list->theme }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
