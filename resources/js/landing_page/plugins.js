$(function() {
    let get_date = $('#countdown').data('event-date');
   /*  const referral = $('#nik').val();
    if (referral) {
        const beginRegisDate = checkRegistrationDate();
        $('.total-vote').text('???');
        if (beginRegisDate < '2021-12-14 09:00:00') {
            $('#registerModal').modal('show');
            // $("#alert_warning").trigger("click", 'Registration is closed right now and will open at 14/Dec/2021 09:00:00');
        } else $('#registerModal').modal('show');
    } */


    checkScroll();
    $(window).on('scroll', checkScroll);
    $('.navbar-toggler').on('click', function() {
        if ($(window).scrollTop() <= 300) {
            $('nav.navbar').toggleClass('solid-toggle');
        }
    });



    if (get_date) {
        $("#countdown").countdown({
            date: get_date,
            /*Change date and time in HTML data-event-date attribute */
            format: "on"
        });
    }

    if ($('body').hasClass('animate-page')) {
        wow = new WOW({
            animateClass: 'animated',
            offset: 100,
            mobile: false
        });
        wow.init();
    }

    if ($('.popup-gallery').length) {
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function(element) {
                    return element.find('img');
                }
            }
        });
    }

    $('#category-carousel').owlCarousel({ //owlCarousel settings
        autoplay: true, //set to false to turn off autoplay and only use nav
        autoplayHoverPause: true, //set to false to prevent pausing on hover
        loop: true, //set to false to stop carousel after all slides shown
        autoplayTimeout: 5000, //time between transitions
        smartSpeed: 1200, //transition speed
        dotsSpeed: 1000, //transition speed when using dots/buttons
        responsive: { //set number of items shown per screen width
            0: {
                items: 1 //0px width and up display 1 item
            },
            768: {
                items: 2 //768px width and up display 2 items
            },
            992: {
                items: 3 //992px width and up display 3 items
            }
        }
    });


    //onScrollInit($('.wow')); //function call with only items
    //onScrollInit($('.staggered-animation'), $('.staggered-animation-container'));

    if ($(window).width() < 768) {
        $('div').attr('data-animation', 'animate__animated animate__fadeInUp');
    }
});

function onScrollInit(items, trigger) {
    items.each(function() { //for every element in items run function
        var osElement = $(this), //set osElement to the current
            osAnimationClass = osElement.attr('data-animation'), //get value of attribute data-animation type
            osAnimationDelay = osElement.attr('data-wow-delay'); //get value of attribute data-delay time

        osElement.css({ //change css of element
            '-webkit-animation-delay': osAnimationDelay, //for safari browsers
            '-moz-animation-delay': osAnimationDelay, //for mozilla browsers
            'animation-delay': osAnimationDelay //normal
        });

        var osTrigger = (trigger) ? trigger : osElement; //if trigger is present, set it to osTrigger. Else set osElement to osTrigger

        osTrigger.waypoint(function() { //scroll upwards and downwards
            osElement.addClass('animated').addClass(osAnimationClass); //add animated and the data-animation class to the element.
        }, {
            triggerOnce: true, //only once this animation should happen
            offset: '70%' // animation should happen when the element is 70% below from the top of the browser window
        });
    });
}

function checkScroll() {
    if ($(window).scrollTop() >= 300) {
        $('.navbar').addClass('solid');
    } else {
        $('.navbar').removeClass('solid');
    }
}

$(document).on('click', 'a[href^="#"]', function(event) {
    event.preventDefault();
    $(".navbar-collapse").removeClass("show");
    $("body").removeClass("offcanvas-active");

    setTimeout(function() {
        $('nav.navbar').removeClass('solid-toggle');
    }, 300);

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
});

$("[data-trigger]").on("click", function() {
    const trigger_id = $(this).attr('data-trigger');
    $(trigger_id).toggleClass("show");
    $('body').toggleClass("offcanvas-active");
});

// close button
$(".btn-close").on('click', function(e) {
    $(".navbar-collapse").removeClass("show");
    $("body").removeClass("offcanvas-active");
});

$(".modal-category").on('click', function(e) {
    e.preventDefault();
    const category = $(this).data('category');

    $.ajax({
            url: '/category',
            data: {
                category: category
            },
            beforeSend: function() {
                $.blockUI({
                    message: 'Please wait....',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });

            },
            success: function() {
                $.unblockUI();
            },
            error: function() {
                $.unblockUI();
            }
        })
        .done(function(data) {
            if (category) $('#categoryLabel').text(category.toUpperCase());
            $('#CategoryTable').html(data);
            $('#categoryModal').modal();
        });
})

function showRegisModal() {
    $('#registerModal').modal('show');
}

function checkRegistrationDate() {
    let time = new Date();
    second = time.getSeconds();
    minute = time.getMinutes();
    hour = time.getHours();
    if (second < 10) {
        second = '0' + second;
    }
    if (minute < 10) {
        minute = '0' + minute;
    }
    if (hour < 10) {
        hour = '0' + hour;
    }

    let _month = parseInt(time.getMonth()) + 1;
    if (_month < 10) _month = '0' + _month;
    else _month;

    let _date = time.getDate();
    if (_date < 10) _date = '0' + _date;
    else _date;

    return `${time.getFullYear()}-${_month}-${_date} ${hour}:00:00`;
}
var form = $('.steps-basic').show();
$('.steps-basic').steps({
    headerTag: 'h6',
    bodyTag: 'fieldset',
    transitionEffect: 'fade',
    titleTemplate: '<span class="number">#index#</span> #title#',
    labels: {
        previous: '<i class="fas fa-arrow-left mr-2"></i> ',
        next: '<i class="fas fa-arrow-right ml-2" />',
        finish: 'Submit <i class="fas fa-paper-plane ml-2" />'
    },
    transitionEffect: 'fade',
            autoFocus: true,
            onStepChanging: function(event, currentIndex, newIndex) {

                if(currentIndex == 0)
                {
                    const nik         = $('#nik').val();
                    const password    = $('#password').val();
                    const phonenumber = $('#phonenumber').val();
                    const check_nik = $('#check_nik').val();

                    if(!nik)
                    {
                        $("#alert_warning").trigger("click", 'Please input NIK first');
                        return false;
                    }

                    if(!password)
                    {
                        $("#alert_warning").trigger("click", 'Please input name first');
                        return false;
                    }

                    if(!phonenumber)
                    {
                        $("#alert_warning").trigger("click", 'Please input phone number first');
                        return false;
                    }
                    if(check_nik==0)
                    {
                        let message = $("#message").val();
                        if (message!='') {
                            $("#alert_warning").trigger("click", message);
                        } else {
                            $("#alert_warning").trigger("click", 'nik checking is not finished');
                        }
                        return false;
                    }



                }
                else if(currentIndex == 1)
                {
                    const igVOted = $('#voted_ig').val();
                    if(!igVOted)
                    {
                        $("#alert_warning").trigger("click", 'Please vote video first');
                        return false;
                    }
                }

                return true;

            },
            onFinished: function(event, currentIndex) {
                const nik         = $('#nik').val();
                const password    = $('#password').val();
                const phonenumber = $('#phonenumber').val();
                const check_nik   = $('#check_nik').val();
                const igVOted     = $('#voted_ig').val();

                if(!nik)
                {
                    $("#alert_warning").trigger("click", 'Please input nik first');
                    return false;
                }

                if(!password)
                {
                    $("#alert_warning").trigger("click", 'Please input password first');
                    return false;
                }

                if(!phonenumber)
                {
                    $("#alert_warning").trigger("click", 'Please input phone number first');
                    return false;
                }

                if(check_nik==0)
                {
                    let message = $("#message").val();
                    if (message!='') {
                        $("#alert_warning").trigger("click", message);
                    } else {
                        $("#alert_warning").trigger("click", 'nik checking is not finished');
                    }
                    return false;
                }
                if(!igVOted)
                {
                    $("#alert_warning").trigger("click", 'Please vote IG first');
                    return false;
                }
                $('#form_regis').trigger('submit');
            }
});
$("#nik").on('change', function() {
    let nik      = $('#nik').val();
    let password = $('#password').val();
    if (nik!=''&&password!='') {
        checkNik(nik,password);
    }
});
$("#password").on('change', function() {
    let nik      = $('#nik').val();
    let password = $('#password').val();
    if (nik!=''&&password!='') {
        checkNik(nik,password);
    }
});
$("#phonenumber").on('change', function() {
    let phone      = $(this).val();
    $("#no-hp-label").val(phone);
});
function checkNik(nik,password){
    $.ajax({
        type: "get",
        url : '/registration/check-nik',
        data: {
            nik: nik,
            password: password,
        },
        success: function(response) {
            $("#message").val('');
            $("#check_nik").val(1);
            $("#nik-label").val(nik);
            $("#name-label").val(response.nama);
            $("#position-label").val(response.position_name);
            $("#unit-label").val(response.unit_name);
            $("#invitation").val(response.invitation);

            return true;
        },
        error: function(response) {
            $("#check_nik").val(0);
            $("#message").val(response.responseJSON.message);
            $("#alert_warning").trigger("click", response.responseJSON.message);
        }
    })
}
$('#form_regis').on('submit', function(event) {
    event.preventDefault();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: $(this).attr('action'),
        data: $(this).serialize(),
        beforeSend: function() {
            $('#registerModal').modal('toggle');
            $(".modal-backdrop").remove();

            $.blockUI({
                message: 'Please wait....',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        error: function(response) {
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
            else $("#alert_error").trigger("click", 'Please contact ICT');

        }
    })
    .done(function(response)
    {
        $.unblockUI();
        
        $("#alert_success").trigger("click", 'Registrasi Berhasil, NIK Anda Sudah Terdaftar Untuk Mendapatkan Kesempatan DoorPrize');
        $('#voted_ig').val('');
        $('.card-ig').removeClass('border-selected');
        $('.card-tikok').removeClass('border-selected');
        $('#form_regis').trigger('reset');
        // window.open(response, '_self');
    });

});

$(".btn-vote-ig").on('click', function() {
    $('.card-ig').removeClass('border-selected');
    $('#voted_ig').val('');
    const id = $(this).data('id');
    $(`#idIG${id}`).addClass('border-selected');
    $('#voted_ig').val(id);

});
$(".btn-vote-tiktok").on('click', function() {
    $('.card-tikok').removeClass('border-selected');
    $('#voted_tiktok').val('');
    const id = $(this).data('id');
    $(`#idTiktok${id}`).addClass('border-selected');
    $('#voted_tiktok').val(id);
});

$(".select-option").select2({
    dropdownParent: $("#registerModal"),
    dropdownPosition: 'below'
});
