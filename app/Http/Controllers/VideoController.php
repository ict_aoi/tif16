<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\videos;
use Storage;

class VideoController extends Controller
{

    public function index()
    {
        return view('admin.videos');
    }

    public function uploadVideo(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'video' => 'required|file',
        ]);

        $video = new Videos;
        $video->title = $request->title;
        if ($request->hasFile('video'))
        {
            $path = $request->file('video')->store('videos', ['disk' => 'my_files']);
            $video->video = $path;
        }
        $video->save();

        return view('admin.videos');

    }
}
