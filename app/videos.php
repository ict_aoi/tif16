<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class videos extends Model
{
    protected $table = 'videos';
    protected $fillable = [
        'title', 'video'
    ];

}
