<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="description" content="{{ env('APP_DESC') }}">
        <meta name="author" content="ICT">
        <meta property="og:url" content="{{ env('APP_URL') }}">
        <meta property="og:description" content="{{ env('APP_DESC') }}">
        <meta property="og:title" content="{{ env('APP_NAME') }}">
        <meta property="og:type" content="website">
        <meta property="og:locale" content="en_ID">

        <meta property="og:image" content="http://bif16.bbi-apparel.com/logo">
        <meta property="og:image:type" content="image/png">
    	<meta property="og:image:width" content="650">
    	<meta property="og:image:height" content="366">


        <meta name="csrf-token" content="{{ csrf_token() }}">


        <title>{{ env('APP_NAME') }}</title>
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <!--link href="{{ mix('css/landing.css') }}" rel="stylesheet" type="text/css"-->

        <script src="{{ mix('js/bootstrap.js') }}"></script>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
        <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
        @yield('content-css')
    </head>

    <body data-spy="scroll" data-target="#navbarResponsive" class="animate-page">
        <noscript>
            <strong>We're sorry but this app doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
        </noscript>

        <div class="container">
            <nav class="navbar navbar-expand-lg fixed-top" id="navbarResponsive">
                <a class="navbar-brand" href="#home"
                    ><img src="{{ asset('images/bif_logo.png') }}" alt="soar to the next level"
                /></a>

                <button
                    class="navbar-toggler"
                    type="button"
                    data-trigger="#mobileNavbar"
                    data-target="#mobileNavbar"
                >
                    <span class="custom-toggler"><i class="fas fa-bars"></i></span>
                </button>

                <div class="collapse navbar-collapse" id="mobileNavbar">
                    <div class="offcanvas-header my-4">
                        <button class="btn btn-outline-danger btn-close float-right" > &times </button>
                        <a class="navbar-brand" href="#home"><img src="{{ asset('images/bif_logo.png') }}" alt="soar to the next level"
                /></a>
                      </div>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a href="{{ route('welcome')}}" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item"><a href="#category" class="nav-link">Category</a></li>
                        <li class="nav-item"><a href="#" class="nav-link btn btn-block btn-primary btn-registration" data-toggle="modal" data-target="#register">Registration</a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <div id="category" class="sub-header-category">
          <div class="container">
            <h3 class="page-title wow fadeInDown">{{ strtoupper($categoryTitle) }}</h3>
          </div>
        </div>

        <div class="container my-3">
          <div class="row">
              <div class="col">
                  <div id="post" class="post-wrap">

                    @foreach ($category as $item)
                      <div class="post-entry">
                        <div class="row">
                            <div class="col-md-8 col-sm-8">
                                <h5 class="post-heading">{{ $item->theme }}</h5>
                                <p class="post-meta">By <a href="#">{{ $item->subco }}</a></p>
                            </div>
                        </div>
                    </div>
                    @endforeach

                  </div>

              </div>

              <!--Blog Sidebar>
              <div class="col-md-3">

                  <div class="sidebar">
                      <div class="sidebar-widget well">
                          <h4>{{ strtoupper($categoryTitle) }}</h4>
                          <p>QCP ITU APA ?</p>
                      </div>
                   </div>

              </div -->
          </div>
      </div>




        <div class="modal fade" id="register" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Register</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  ...
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>


        <script src="{{ mix('js/plugins.js') }}"></script>
    </body>
</html>
