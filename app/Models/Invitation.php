<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $fillable = [
        'subco',
        'name',
        'birth_day',
        'referral_code',
        'phone_number',
        'qr_image',
        'voted_at'
    ];

}
