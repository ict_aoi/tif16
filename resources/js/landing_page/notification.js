let SweetAlert = function() {



    let _componentSweetAlert = function() {
        if (typeof swal == 'undefined') {
            console.warn('Warning - sweet_alert.min.js is not loaded.');
            return;
        }

        // Defaults
        let swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });


        $('#alert_success').on('click', function(event, arg1) {
            swalInit({
                title: 'Good job!',
                text: arg1,
                type: 'success',
                timer: 2000,
                showCloseButton: true,
                showCancelButton: false,
                showConfirmButton: false,
            });
        });

        // Error alert
        $('#alert_error').on('click', function(event, arg1) {
            swalInit({
                title: 'Oops...',
                text: 'Something went wrong!',
                type: 'error'
            });
        });

        // Warning alert
        $('#alert_warning').on('click', function(event, arg1) {
            swalInit({
                title: arg1,
                type: 'warning',
                icon: '<i class="fas fa-clock"></i>',
                timer: 3000,
                showCloseButton: true,
                showCancelButton: false,
                showConfirmButton: false,
            });
        });

        // Info alert
        $('#alert_info').on('click', function(event, arg1) {
            swalInit({
                title: 'For your information',
                text: arg1,
                type: 'info'
            });
        });
    };




    //
    // Return objects assigned to module
    //

    return {
        initComponents: function() {
            _componentSweetAlert();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    SweetAlert.initComponents();
});