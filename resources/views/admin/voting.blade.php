@extends('admin.template')

@section('content')

    <!-- Main Content -->
    <div id="content">

        @include('admin.top_bar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Vote Result</h1>
            </div>

            <!-- Content Row -->
            <div class="row">

                @foreach ($voting as $value)

                <div class="col-lg-4 mb-3 d-flex align-items-stretch">
                    <div class="card">
                        <video controls>
                            <source src="{{ route('getVideoAdmin', $value->file_name)  }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <div class="card-body">
                          <h5 class="card-title">{{ strtoupper(str_replace('_', ' ', $value->category)) }}</h5>
                          <h5 class="card-text">TOTAL VOTE : {{ $value->total_vote }}</h5>
                        </div>
                    </div>
                </div>

                @endforeach

            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

@endsection
