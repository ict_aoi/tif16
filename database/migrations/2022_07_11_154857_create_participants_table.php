<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nik')->nullable();
            $table->string('name');
            $table->string('unit_name');
            $table->string('position_name');
            $table->string('factory');
            $table->string('password');
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->timestamp('registration_at')->nullable();
            $table->timestamp('voted_at')->nullable();
            $table->string('lottery_number')->nullable();
            $table->boolean('is_invitation')->default(false);
            $table->boolean('is_exclude')->default(false);
            $table->boolean('is_get_doorprize')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}