<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Voter extends Model
{
    protected $fillable = [
        'category',
        'file_name',
    ];

    protected $appends = [
        'voter_url',
    ];


    public function getVoterUrlAttribute()
    {
        return route('showVoterFile',$this->attributes['file_name']);
    }

    public function getTotal()
    {
        return $this->hasMany('App\Models\PollingBooth','voter_id')
        ->selectRaw("
           count(0) as total,
           voter_id")
        ->groupby('voter_id');
    }



}
