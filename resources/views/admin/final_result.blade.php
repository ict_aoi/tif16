<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible"  content="IE=edge">
      <meta name="discription" content="Student Result System">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

      <title>Admin panel</title>
      <style>
        *{
            font-family: 'Poppins', sans-serif;
            font-weight: 500;
        }
        body{
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
            background-color: #AB46D2;
            overflow:hidden;
        }
        .up{
            width: 300px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-size: 2rem;
            color: #fff;
            height: 300px;
            border-radius: 50%;
            background-color: #FF6FB5;
            box-shadow: 1px -5px 2px 5px #FFCD38;
            transform: rotate(30deg);
            animation: load 1s linear infinite reverse;
        }

        h2{
            position: absolute;
            color: #fff;
            top:47%;
            left:50%;
            font-size: 2.5rem;
            transform: translate(-50%,-50%);
            z-index: 5;
        }

        .up::after{
            content: '';
            top: 70%;
            left: 8px;
            width: 10px;
            height: 10px;
            position: absolute;
            background-color: #FFCD38;
            border-radius: 50%;
            box-shadow: 0 0 1px 2px #FFCD38;


        }

        @keyframes load {
            0%{
                transform: rotate(0deg);
                box-shadow: 0 0 1px 0px black;
            }
            100%{
                transform: rotate(360deg);
            }
        }
      </style>

  </head>
  <body>
    <h2>0%</h2>
    <span class="up">
    <span id = "rote"></span>
    </span>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        var count = 0;
        function lop (){
            if(count <= 100){
                setInterval(x,40);
            }else
            {
                console.log(count);
            }
        }
        lop();

        function x (){
            if(count <= 100){
                $('h2').html(count++ +"%");
            }
            else {
                count = 100;
                $('h2').html(count+"%");
                return true;
            }
        }

        setTimeout("location.href = '{{ route('bestVoteAdmin') }}';",4010);

    });
    </script>
</body>

</html>
