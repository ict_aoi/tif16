<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', function () {
    return redirect('/total-voted');
});

Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/voter/file-name/{filename}', 'HomeController@showVoterFile')->name('showVoterFile');
Route::get('/logo', 'HomeController@logo')->name('logo');
Route::get('/category', 'HomeController@category')->name('category');
Route::post('/registration', 'HomeController@registration')->name('registration');
Route::get('/registration/check-nik', 'HomeController@checkNik')->name('registration_check_nik');
Route::get('/download-registered', 'HomeController@downloadRegistered')->name('downloadRegistered');

Route::group(['middleware' => ['admin']], function () {
    Route::get('/home-admin', 'AdminHomeController@index')->name('homeAdmin'); //home admin
    Route::get('/final_result', 'AdminHomeController@finalResult')->name('finalResultAdmin'); //get vote
    Route::get('/best_vote', 'AdminHomeController@bestVote')->name('bestVoteAdmin'); //get vote
    Route::get('/voting', 'AdminHomeController@voting')->name('votingAdmin'); //get vote
    Route::get('/spinner', 'AdminHomeController@spinner')->name('spinnerAdmin');
    Route::get('/spinner_viewers', 'AdminHomeController@spinnerPenonton')->name('spinnerPenontonAdmin');
    Route::post('/get_number', 'AdminHomeController@GetNum')->name('getNumAdmin'); //get data undangan
    Route::post('/get_number_penonton', 'AdminHomeController@GetNumPenonton')->name('getNumPenontonAdmin'); //get data pentntn
    Route::post('/get_prize', 'AdminHomeController@getPrize')->name('getPrizeAdmin'); //update sudah dapet hadiah
    Route::post('/row_number', 'AdminHomeController@GetRow')->name('getRowAdmin');

    Route::get('/get-video/{video}', 'AdminHomeController@getVideo')->name('getVideoAdmin'); //get video
});

Route::get('/video-photo', 'VideoController@index')->name('videos.index');
Route::post('/uploadVideo', 'VideoController@uploadVideo')->name('videos.uploadedVideo');

Auth::routes();
Route::get('/invitation/{id}', 'HomeController@invitation')->name('invitation');
// Route::get('/generate-invitation', 'HomeController@generateInvitation')->name('generate_invitation');

Route::get('/partisipans', 'PartisipanController@index')->name('partisipan_index'); //home admin
Route::post('/partisipans/upload-partisipans', 'PartisipanController@uploadFormPartisipan')->name('partisipans_upload'); //home admin
 //home admin