<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polling_booths', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('voter_id')->nullable();
            $table->unsignedBigInteger('participant_id')->nullable();
            $table->foreign('voter_id')->references('id')->on('voters')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('participant_id')->references('id')->on('participants')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votings');
    }
}