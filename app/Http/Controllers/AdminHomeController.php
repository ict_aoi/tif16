<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class AdminHomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $participantsAll = DB::table('participants')
                            ->where('is_invitation', true)
                            ->count();

        $registrasionAll = DB::table('participants')
                            ->where('is_invitation', true)
                            ->whereNotNull('registration_at')
                            ->count();

        $votedAll = DB::table('participants')
                            ->where('is_invitation', true)
                            ->whereNotNull('registration_at')
                            ->whereNotNull('voted_at')
                            ->count();

        $participantsYoutube = DB::table('participants')
                            ->where('is_invitation', false)
                            ->count();

        $registrasionYoutube = DB::table('participants')
                            ->where('is_invitation', false)
                            ->whereNotNull('registration_at')
                            ->count();

        $votedYoutube = DB::table('participants')
                            ->whereNotNull('registration_at')
                            ->whereNotNull('voted_at')
                            ->where('is_invitation', false)
                            ->count();

        $notVotedAll = $participantsAll - $votedAll;

        return view('admin.index', compact('participantsAll', 'registrasionAll', 'votedAll', 'notVotedAll', 'participantsYoutube', 'registrasionYoutube', 'votedYoutube'));
    }

    public function voting()
    {
        $voting = DB::select("SELECT
                                    voters.id,
                                    (SELECT count(voter_id) FROM polling_booths WHERE voter_id = voters.id) AS total_vote,
                                    voters.category,
                                    voters.file_name
                                FROM
                                    voters
                                ORDER BY total_vote DESC
                            ");

        return view('admin.voting', compact('voting'));
    }

    public function spinner()
    {
        $invitation = DB::table('participants')
                    ->where([
                        'is_invitation'   => true,
                        'is_exclude'      => false,
                        'is_get_doorprize'=> true
                    ])
                    ->whereNotNull([
                        'registration_at', 'voted_at'
                    ])
                    ->get();

        $ada_row    = DB::table('row')->first();

        if ($ada_row != NULL) {
            $row_default = $ada_row->row;
        } else {
            $row_default = 1;
        }

        return view('admin.spinner', compact('invitation', 'row_default'));
    }

    public function spinnerPenonton()
    {
        $invitation = DB::table('participants')
                    ->where([
                        'is_invitation'   => false,
                        'is_exclude'      => false,
                        'is_get_doorprize'=> true
                    ])
                    ->whereNotNull([
                        'registration_at', 'voted_at'
                    ])
                    ->get();

        $ada_row    = DB::table('row')->first();

        if ($ada_row != NULL) {
            $row_default = $ada_row->row;
        } else {
            $row_default = 1;
        }

        return view('admin.spinner_penonton', compact('invitation', 'row_default'));
    }

    public function getPrize(Request $request)
    {
        $ids = $request->ids;

        $test = DB::table('participants')
        ->whereIn('id', $ids)
        ->update(['is_get_doorprize' => 1]);

        response()->json(['success' => 'success'], 200);

    }

    public function GetNum()
    {
        $query_undian = DB::table('participants')
                        ->whereNotNull('registration_at')
                        ->whereNotNull('voted_at')
                        ->where('is_invitation', true)
                        ->where('is_exclude', false)
                        ->where('is_get_doorprize', false)
                        ->get();

        echo json_encode($query_undian);
    }

    public function GetNumPenonton()
    {
        $query_undian = DB::table('participants')
                        ->whereNotNull('registration_at')
                        ->whereNotNull('voted_at')
                        ->where('is_invitation', false)
                        ->where('is_exclude', false)
                        ->where('is_get_doorprize', false)
                        ->get();

        echo json_encode($query_undian);
    }

    public function GetRow(Request $request)
    {

        DB::table('row')->delete();

        DB::table('row')->insert([
            'row' => $request->jml_row
        ]);

    }

    public function getVideo($video)
    {
        // $name = $video->file_name;
        $fileContents = Storage::disk('local')->get("voters/{$video}");
        $response = \Response::make($fileContents, 200);
        $response->header('Content-Type', "video/mp4");
        return $response;
    }

    public function finalResult()
    {
        return view('admin.final_result');
    }

    public function bestVote()
    {
        $voting = DB::select("SELECT
                                    voters.id,
                                    (SELECT count(voter_id) FROM polling_booths WHERE voter_id = voters.id) AS total_vote,
                                    voters.category,
                                    voters.file_name
                                FROM
                                    voters
                                ORDER BY total_vote DESC
                            ");

        return view('admin.best_vote', compact('voting'));
    }

}
