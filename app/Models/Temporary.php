<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Temporary extends Model
{
    protected $fillable     = ['column_1'
    ,'column_2'
    ,'column_3'
    ,'column_4'
    ];
    protected $dates     = ['created_at'];
}