<?php

namespace App\Console\Commands;

use DB;
use StdClass;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Http\Controllers\HomeController;

class IncaseEvent extends Command
{
    protected $signature = 'IncaseEvent:update';
    protected $description = 'Update row based on case';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $running_at = carbon::now();
        HomeController::generateInvitation();
    }
}