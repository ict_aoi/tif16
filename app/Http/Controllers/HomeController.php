<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use File;
use Config;
use DB;
use Excel;
use Carbon\Carbon;

use App\Models\Voter;
use App\Models\Employee;
use App\Models\Invitation;
use App\Models\Participant;
use App\Models\CategoryTheme;
use App\Models\PollingBooth;

use SimpleSoftwareIO\QrCode\Facades\QrCode;

class HomeController extends Controller
{
    public function __construct()
    {
        /* $this->middleware('auth')->except([
            'logo',
            'category',
            'invitation',
            'welcome',
            'registration',
            'showVoterFile'
        ]); */
    }

    public function logo()
    {
        $resp = response()->download(public_path('images/bif_logo.png'));
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    // public function totalVoted(Request $request)
    // {
    //     $resultTiktos = PollingBooth::with([
    //         'voter'
    //     ])
    //     ->whereHas('voter',function ($query)
    //     {
    //         $query->where('category','tiktok');
    //     })
    //     ->select([
    //         'voter_id',
    //         db::raw("count(0) as total")
    //     ])
    //     ->groupby('voter_id')
    //     ->orderby(db::Raw("count(0)"),'desc')
    //     ->get();


    //     $resultIgs = PollingBooth::with([
    //         'voter'
    //     ])
    //     ->whereHas('voter',function ($query)
    //     {
    //         $query->where('category','instagram');
    //     })
    //     ->select([
    //         'voter_id',
    //         db::raw("count(0) as total")
    //     ])
    //     ->groupby('voter_id')
    //     ->orderby(db::Raw("count(0)"),'desc')
    //     ->get();

    //     // return view('home',compact('resultTiktos','resultIgs'));
    //     return view('admin.index');
    // }

    public function welcome(Request $request)
    {
        $referral       = $request->referral;
        $tiktokVoters   = Voter::inRandomOrder()->get();
        return view('welcome',compact('referral','tiktokVoters'));
    }

    public function registration(Request $request)
    {
        $nik          = trim($request->nik);
        $password     = trim($request->password);
        $current_date = carbon::now()->todatetimestring();
        $participant = Participant::where('nik',$nik)->first();
        // dd($request->all());
        if ($participant) {
            if ($participant->password==$password) {
                if (!$participant->registration_at) {
                    if ($current_date >= '2022-07-13 00:00:01') {
                        if ($current_date <= '2022-07-16 09:00:00') {
                            $dataEmployee = $participant;
                            $invitation   = true;
                        } else {
                            return response()->json(['message' => 'Registrasi sudah tutup'],422);
                        }
                    }else{
                        return response()->json(['message' => 'Registrasi Belum di buka'],422);
                    }
                    
                } else {
                    return response()->json(['message' => 'Nik '.$participant->nik.' already register at '.$participant->registration_at],422);
                }
            } else {
                return response()->json(['message' => 'Password Salah'],422);
            }
        }else{
            if ($current_date >= '2022-07-16 07:00:00') {
                if ($current_date <= '2022-07-16 10:36:00') {
                    $participant = Employee::where([
                                        ['empid',$nik]
                                    ])
                                    ->first();
                    if ($participant) {
                        if ($participant->password==$password) {
                            $checkParticipant = Participant::where('nik',$nik)->first();
                            if (!$checkParticipant) {
                                $dataEmployee = $participant;
                                $invitation   = false;
                            }else {
                                return response()->json(['message' => 'Nik '.$checkParticipant->empid.' sudah register pada '.$checkParticipant->registration_at],422);
                            }
                            
                        } else {
                            return response()->json(['message' => 'password salah'],422);
                        }
                    } else {
                        return response()->json(['message' => 'nik tidak di temukan'],422);
                    }
                } else {
                    return response()->json(['message' => 'Registrasi untuk umum sudah tutup'],422);
                }
            } else {
                return response()->json(['message' => 'Registrasi untuk umum baru dibuka saat acara BIF dimulai hari sabtu pukul 07.00'],422);
            }
        }
        if ($dataEmployee) {
            try {
                DB::beginTransaction();
                if ($invitation) {
                    DB::table('participants')
                    ->where('nik',$nik)
                    ->update([
                        'registration_at' => Carbon::now(),
                        'voted_at'        => Carbon::now(),
                        'phone_number'    => $request->phonenumber,
                    ]);
                    $partisipanId   = $dataEmployee->id;
                    $lottery_number = $dataEmployee->lottery_number;
                } else {
                    $lottery_number= self::random();
                    $participant = Participant::create([
                        'nik'             => $nik,
                        'name'            => $dataEmployee->name,
                        'unit_name'       => $dataEmployee->unit_name,
                        'position_name'   => $dataEmployee->position_name,
                        'factory'         => $dataEmployee->factory,
                        'password'        => $dataEmployee->password,
                        'phone_number'    => $request->phonenumber,
                        'lottery_number'  => $lottery_number,
                        'voted_at'        => Carbon::now(),
                        'registration_at' => Carbon::now(),
                        'is_invitation'   => $invitation,
                    ]);
                    $partisipanId = $participant->id;
                }
                PollingBooth::FirstOrCreate([
                    'voter_id'        => $request->voted_ig,
                    'participant_id' => $partisipanId,
                    'created_at'     => Carbon::now(),
                    'updated_at'     => Carbon::now()
                ]);
                DB::commit();
                // $url = route('invitation',$nik);
                return response()->json('Registrasi Berhasil, NIK Anda Sudah Terdaftar Untuk Mendapatkan Kesempatan DoorPrize',200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
        } else {
            return response()->json(['message' => 'nik not found'],422);
        }


    }

    public function checkNik(Request $request)
    {
        $nik          = trim($request->nik);
        $password     = trim($request->password);
        $current_date = carbon::now()->todatetimestring();
        $participant = Participant::where('nik',$nik)->first();
        
        if ($participant) {
            if ($participant->password==$password) {
                if (!$participant->registration_at) {
                    $dataEmployee = $participant;
                    $invitation   = true;
                } else {
                    return response()->json(['message' => 'Nik '.$participant->nik.' already register at '.$participant->registration_at],422);
                }
            } else {
                return response()->json(['message' => 'password wrong'],422);
            }
        }else{
            if ($current_date >= '2022-07-16 07:00:00') {
                if ($current_date <= '2022-07-16 10:36:00') {
                    $participant = Employee::where([
                            ['empid',$nik]
                        ])
                        ->first();
                    if ($participant) {
                        if ($participant->password==$password) {
                            $dataEmployee=$participant;
                            $invitation   = false;
                        } else {
                            return response()->json(['message' => 'password salah'],422);
                        }
                    } else {
                        return response()->json(['message' => 'nik tidak ditemukan'],422);
                    }
                }else{
                    return response()->json(['message' => 'Registrasi untuk umum sudah tutup'],422);
                }
            } else {
                return response()->json(['message' => 'Registrasi untuk umum baru dibuka saat acara BIF dimulai hari sabtu pukul 07.00'],422);
            }
        }
        if ($dataEmployee) {
            $nama          = $dataEmployee->name;
            $unit_name     = $dataEmployee->unit_name;
            $position_name = $dataEmployee->position_name;
            $area          = $dataEmployee->area;
            return response()->json([
                'nama'          => $nama,
                'unit_name'     => $unit_name,
                'position_name' => $position_name,
                'area'          => $area,
                'invitation'    => $invitation,
            ],200);
        } else {
            return response()->json(['message' => 'nik tidak ada'],422);
        }
                    
    }
    static function random()
    {
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

                
            $referral_code  = Carbon::now()->format('u');
            $lottery_number = $referral_code+Participant::where('lottery_number',$referral_code)->count() + 1;
            $try -= 1;
            
        } while (Participant::where('lottery_number',$lottery_number)->exists());

        return $lottery_number;
    }
    public function category(Request $request)
    {
        $categoryTitle = $request->category;

        $lists = CategoryTheme::where('category',$categoryTitle)
        ->orderby('subco','asc')
        ->get();

        return view('_lov_category',compact('lists'));
    }

    public function invitation(Request $request,$id)
    {
        $invitations = Participant::whereNotNull('registration_at')
        ->where('nik',$id)
        ->first();
        if (!$invitations) {
            return response()->json(['message' => 'nik not found'],422);
        }
        
        $lottery_number = $invitations->lottery_number;
        if ($invitations->is_invitation) {
            $file       = Config::get('storage.invitation') . '/BIF17_'.$id.'.pdf';
            if(!file_exists($file)){
                $pdf = Pdf::loadView('invitation',compact('lottery_number'));
                return $pdf->download('BIF17_'.$id.'.pdf');
            }
            $resp = response()->download($file);
            $resp->headers->set('Pragma', 'no-cache');
            $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
            $resp->headers->set('X-Content-Type-Options', 'nosniff');
            return $resp;
        } else {
            $pdf = Pdf::loadView('invitation',compact('lottery_number'));
            return $pdf->download('BIF17_'.$id.'.pdf');
        }

    }
    static function generateInvitation()
    {
        $invitations = Participant::get();
        $file       = Config::get('storage.invitation');
        foreach ($invitations as $key => $inv) {
            $lottery_number = $inv->lottery_number;
            PDF::loadView('invitation',compact('lottery_number'))->save($file.'/BIF17_'.$inv->nik.'.pdf');
        }
    }

    public function showVoterFile($filename)
    {
        $path = Config::get('storage.voters');
        $resp = response()->download($path.'/'.$filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function downloadRegistered()
    {
        $invitations = Invitation::whereNotNuLL('voted_at')->get();
        return Excel::create('data_registration',function ($excel) use ($invitations)
        {
            $excel->sheet('active', function($sheet) use ($invitations)
            {

                $sheet->setCellValue('A1','QR_CODE');
                $sheet->setCellValue('B1','SUBCO');
                $sheet->setCellValue('C1','NAME');
                $sheet->setCellValue('D1','PHONE_NUMBER');
                $sheet->setCellValue('E1','REGISTER_AT');

                $row    = 2;
                foreach ($invitations as $key => $value) {

                    $sheet->setCellValue('A'.$row,$value->referral_code);
                    $sheet->setCellValue('B'.$row,$value->subco);
                    $sheet->setCellValue('C'.$row,$value->name);
                    $sheet->setCellValue('D'.$row,$value->phone_number);
                    $sheet->setCellValue('E'.$row,$value->voted_at);

                    $row++;
                }
            });

        })
        ->export('xlsx');
    }
}