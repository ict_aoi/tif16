<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable  = ['empid','name','unit_name','position_name','factory','password'];
	protected $dates     = ['created_at'];
}