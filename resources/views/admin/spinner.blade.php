@extends('admin.template')

@section('content')

    <!-- Main Content -->
    <div id="content">

        @include('admin.top_bar')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Lucky Spin For Invitation</h1>
                <div class="col-xs-2">
                    <input class="form-control" id="rowNumber" type="number" min="1" placeholder="Input Row Number" value="{{ $row_default }}">
                  </div>
            </div>

            <!-- Content Row -->
            <div class="row">

                <div class="col-lg-12 mb-12 text-center">

                    <div class="card shadow mb-4">
                        <div class="card-body">

                            <div id="info">CLICK START TO BEGIN</div>
                            <div id="nomor">
                              @php

                              for ($i=0; $i < $row_default; $i++) {
                                echo '<h3 id="row_'.$i.'">--</h3>';
                                echo '<input id="id_'.$i.'" type="hidden">';
                              }

                              @endphp
                            </div>
                            <input id="id" type="hidden">
                            <div id="button">
                              <button id="start" class="btn btn-lg btn-success" type="button" name="button"><i class="fa fa-play"></i> Start</button>
                              <button id="stop" class="btn btn-lg btn-danger" type="button" name="button"><i class="fa fa-stop"></i> Stop</button>
                              <button id="reset" class="btn btn-lg btn-default" type="button" name="button"><i class="fa fa-refresh"></i> Reset</button>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

@endsection

@section('js')
<script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script type="text/javascript" src="{{ asset('/js/Winwheel.js') }}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#rowNumber').on('change',function(){
        const chunk = document.getElementById("rowNumber").value;

        $.ajax({
            url:"/row_number",
            method:"POST", //First change type to method here
            data:{
              jml_row: chunk,
            },
            success:function(response) {
             console.log(chunk);
             location.reload();
           },
           error:function(){
            alert("error");
           }

        });

     });

      var i,j,temp,pop,col,chunk = {{ $row_default }};
      var rand;
      var idx = [];
      var chosen = [];
      var res = [];

      function getCol(matrix, col){
         var column = [];

         for(var i=0; i<matrix.length; i++){
            column.push(matrix[i][col]);
         }

         return column;
      }

      function chunkArray(myArray, chunk_size){
          var index = 0;
          var arrayLength = myArray.length;
          var tempArray = [];
          var col = [];
          var res = new Array();

          for (index = 0; index < arrayLength; index += chunk_size) {
              myChunk = myArray.slice(index, index+chunk_size);
              // Do something if you want with the group
              tempArray.push(myChunk);
          }

          for (var i = 0; i < chunk_size; i++) {
            col[i] = getCol(tempArray,i);
          }

          return col;
      }

      function get_random(i){

        return setInterval(function () {
          var id = Math.floor(Math.random()*pop[i].length);
          $("#row_"+i).html(pop[i][id]['nik']+' ('+pop[i][id]['name']+')');
          $("#id_"+i).val(pop[i][id]['id']);
        }, 10);
      }

      //initial hide
      $("#stop").hide();
      $("#reset").hide();

      //starting
      $("#start").click(function () {
        $("#start").hide();
        $("#stop").show();
        $("#info").html("PROCESSING ...");
        $.ajax({
          type : 'post',
          url : '/get_number',
          dataType : 'json',
          async : 'false',
          success : function (data) {
            temp = data;
            if(temp.length == 0){
              $("#stop").hide();
              $("#start").show();
              for (var i = 0; i < chunk; i++) {
                $("#row_"+i).html('--');
                $("#id_"+i).val('');
              }
              $("#info").html("CLICK START TO BEGIN");
              alert('All data have been chosen!');
            }else{
              pop = chunkArray(temp, chunk);
              for (var i = 0; i < chunk; i++) {
                res[i] = get_random(i);
              }
            }
          }
        })
      })

    //stopping
      $("#stop").click(function () {
        $("#stop").hide();
        $("#reset").show();

        var ids = new Array();

        for (var i = 0; i < chunk; i++) {
          clearInterval(res[i]);
          ids.push($("#id_"+i).val());
        }

        $.ajax({
            type: "POST",
            url: "{{ route('getPrizeAdmin') }}",
            data: {
                ids:ids
            },
            success: function (response) {
                console.log("sukses");
            },
            error: function (response) {
                console.log(response);
            },
            dataType: 'json'

        });

        $("#info").html("THE CHOSEN IS");
      })

      $("#reset").click(function () {
        $("#start").show();
        $("#reset").hide();
        for (var i = 0; i < chunk; i++) {
          $("#row_"+i).html('--');
          $("#id_"+i).val('');
        }
        $("#info").html("CLICK START TO BEGIN");
      })
</script>
@endsection
