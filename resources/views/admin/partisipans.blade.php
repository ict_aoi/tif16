@extends('admin.template')

@section('content')
    <div id="content">
        @include('admin.top_bar')
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div
                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Partisipans</h6>
                            <div class="dropdown no-arrow">
                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                    aria-labelledby="dropdownMenuLink">
                                    <div class="dropdown-header">Menu</div>
                                    <a class="dropdown-item" href="javascript:void(0)" onclick="syncEmployee()">Sync Employee</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Add</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void(0)" onclick="showUploadPartisipanModal()">Upload Partisipans</a>
                                </div>
                            </div>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="chart-area">
                                <div class="table-responsive">
                                    <table class="table table-hover" id="dtable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NIK</th>
                                                <th>NAME</th>
                                                <th>FACTORY</th>
                                                <th>TYPE INVITATION</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="uploadPartisipans" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-labelledby="uploadPartisipansLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header bg-tif">
            <h5 class="modal-title" id="uploadPartisipansLabel">Upload Partisipans</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {!!
                Form::open([
                    'role'    => 'form',
                    'url'     => route('partisipans_upload'),
                    'method'  => 'POST',
                    'id'      => 'upload_file_allocation',
                    'enctype' => 'multipart/form-data'
                ])
            !!}
                <input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                <button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
            {!! Form::close() !!}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
@endsection

@section('js')
<script>
    $( document ).ready(function() {
    });
    function syncEmployee(){
        $.ajax({
            type: "get",
            url : '',
            
            success: function() {
                
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
            }
        })
        .done(function() {
            // $("#btnCopyProsesClose").trigger("click");
            $("#alert_success").trigger("click", 'Data Berhasil di Copy');
        });
    }
    function showUploadPartisipanModal() {
        $('#uploadPartisipans').modal('show');
    }
    $('#upload_file').on('change', function() {
        //$('#upload_file_allocation').submit();
        $.ajax({
                type: "post",
                url: $('#upload_file_allocation').attr('action'),
                data: new FormData(document.getElementById("upload_file_allocation")),
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
                    $.unblockUI();
                    
    
                },
                error: function(response) {
                    $.unblockUI();
                    $('#upload_file_allocation').trigger('reset');
                    
    
                }
            })
            .done(function() {
               
            });
    
    })
</script>
@endsection
