<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-bif sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">BIF-17 ADMIN</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('homeAdmin') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item">
        <a target="_blank" class="nav-link" href="{{ route('finalResultAdmin') }}">
            <i class="fas fa-gift"></i>
            <span>Final Result</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('votingAdmin') }}">
            <i class="fas fa-solid fa-video"></i>
            <span>Voting</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSpinner">
            <i class="fas fa-fw fa-cog"></i>
            <span>Spinner</span>
        </a>
        <div id="collapseSpinner" class="collapse" aria-labelledby="headingSpinner"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Sub Menu:</h6>
                <a class="collapse-item" href="{{ route('spinnerAdmin') }}">Undangan</a>
                <a class="collapse-item" href="{{ route('spinnerPenontonAdmin') }}">Penonton</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-photo-video"></i>
            <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Sub Menu:</h6>
                <a class="collapse-item" href="{{ route('videos.index') }}">Upload</a>
                <a class="collapse-item" href="utilities-border.html">List</a>
            </div>
        </div>
    </li>
    <div class="sidebar-heading">
        Back End
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('partisipan_index') }}">
            <i class="fas fa-fw fa-cog"></i>
            <span>Partisipans</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
