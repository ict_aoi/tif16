<?php use Illuminate\Database\Seeder;

use App\Models\Invitation;
use Carbon\Carbon;

use Faker\Factory as Faker;

class InvitationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=38; $i < 38; $i++) { 
            DB::table('invitations')
            ->insert([
                'subco'         => 'AMA',
                'referral_code' => $faker->ean8,
            ]);
        }
        
    }
}
