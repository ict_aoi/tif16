const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .scripts([
        'resources/js/limitless/libraries/ripple.min.js',
        'resources/js/limitless/libraries/uniform.min.js',
        'resources/js/plugins/forms/styling/uniform.min.js',
        'resources/js/plugins/ui/moment/moment.min.js',
        'resources/js/plugins/extensions/mousewheel.min.js',
        'resources/js/plugins/table/datatables/datatables.min.min.js',
        'resources/js/limitless/libraries/layout_fixed_sidebar_custom.js',
        'resources/js/limitless/libraries/sweet_alert.min.js',
        'resources/js/plugins/ui/prism.min.js',
        'resources/js/plugins/ui/sticky.min.js',
        'resources/js/plugins/ui/perfect_scrollbar.min.js',
        'resources/js/limitless/app.js',
    ], 'public/js/limitless.js')
    .scripts([
        'resources/js/boostrap/jquery-3.4.1.min.js',
        'resources/js/boostrap/popper.min.js',
        'resources/js/boostrap/bootstrap.bundle.min.js',
    ], 'public/js/bootstrap.js')
    .scripts([
        'resources/js/landing_page/owl.carousel.min.js',
        'resources/js/landing_page/countdown.js',
        'resources/js/landing_page/wow.js',
        'resources/js/landing_page/magnific-popup.js',
        'resources/js/landing_page/jquery.waypoints.min.js',
        'resources/js/landing_page/select2.min.js',
        'resources/js/landing_page/steps.min.js',
        'resources/js/landing_page/sweet_alert.min.js',
        'resources/js/landing_page/blockui.min.js',
        'resources/js/landing_page/notification.js',
        'resources/js/landing_page/plugins.js',
    ], 'public/js/plugins.js')
    .styles([
        'resources/css/bootstrap/bootstrap.css',
    ], 'public/css/bootstrap.css')
    .styles([
        'resources/css/landing_page/owl.carousel.min.css',
        'resources/css/landing_page/owl.theme.default.min.css',
    ], 'public/css/carousel.css')
    .styles([
        'resources/css/landing_page/magnific-popup.css',
    ], 'public/css/magnific-popup.css')
    .styles([
        'resources/css/landing_page/animate.css',
    ], 'public/css/animate.css')
    .styles([
        'resources/css/landing_page/style.css',
    ], 'public/css/landing.css')
    .styles([
        'resources/css/limitless/bootstrap_limitless.css',
        'resources/css/limitless/layout.css',
        'resources/css/limitless/components.css',
        'resources/css/limitless/colors.css',
        'resources/css/limitless/custom.css'
    ], 'public/css/limitless.css')
    .copyDirectory('resources/images', 'public/images')
    .copyDirectory('resources/css/icons', 'public/css/icons')
    .sass('resources/sass/app.scss', 'public/css')
    .copy(
        'node_modules/@fortawesome/fontawesome-free/webfonts',
        'public/webfonts'
    );