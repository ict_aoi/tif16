<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="description" content="{{ env('APP_DESC') }}">
        <meta name="author" content="ICT">
        <meta property="og:url" content="{{ env('APP_URL') }}">
        <meta property="og:description" content="{{ env('APP_DESC') }}">
        <meta property="og:title" content="{{ env('APP_NAME') }}">
        <meta property="og:type" content="website">
        <meta property="og:locale" content="en_ID">
        <meta property="og:image" content="{{ asset('images/bif_logo.png') }}">
        <meta property="og:image:type" content="image/jpg">
    	  <meta property="og:image:width" content="650">
    	  <meta property="og:image:height" content="366">
        <meta name="keywords" content="tif,tif16,triputra improvement forum, tif,soar,next level,tifxiv">

        <meta name="csrf-token" content="{{ csrf_token() }}">


        <title>{{ env('APP_NAME') }}</title>
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <!--link href="{{ mix('css/landing.css') }}" rel="stylesheet" type="text/css"-->

        <script src="{{ mix('js/bootstrap.js') }}"></script>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
        <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
        @yield('content-css')
    </head>

    <body data-spy="scroll" data-target="#navbarResponsive" class="animate-page" style="background-color: #Eddecc;">
        <noscript>
            <strong>We're sorry but this app doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
        </noscript>

        <section id="home">
            <div class="container">
                <nav class="navbar navbar-expand-lg fixed-top" id="navbarResponsive">


                    <button
                        class="navbar-toggler"
                        type="button"
                        data-trigger="#mobileNavbar"
                        data-target="#mobileNavbar"
                    >
                        <span class="custom-toggler"><i class="fas fa-bars"></i></span>
                    </button>

                    <a class="navbar-brand mr-auto" href="#home"
                      ><img src="{{ asset('images/bif_logo.png') }}" alt="soar to the next level"
                  /></a>

                  <a class="nav-link btn btn-primary btn-registration btn-regis-mobile ml-auto" type="button" onClick="showRegisModal()">Registration</a>

                    <div class="collapse navbar-collapse" id="mobileNavbar">
                        <div class="offcanvas-header my-4">
                            <button class="btn btn-outline-danger btn-close float-right" > &times </button>
                            <a class="navbar-brand" href="#home"><img src="{{ asset('images/bif_logo.png') }}" alt="soar to the next level"
                    /></a>
                          </div>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a href="#home" class="nav-link">Home</a>
                            </li>
                            <li class="nav-item"><a href="#gallery" class="nav-link">Gallery</a></li>
                            <li class="nav-item"><a href="#sponsor" class="nav-link">Sponsors</a></li>
                            <li class="nav-item"><a class="nav-link btn btn-block btn-primary btn-registration"  onClick="showRegisModal()">Registration</a></li>
                        </ul>
                    </div>
                </nav>
            </div>

            <div id="carouselExampleIndicators" class="carousel slide header-carousel" data-ride="carousel" data-interval="8000">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    {{--  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>  --}}
                </ol>
                <div class="carousel-inner" role="listbox">
                    <!-- Slide One -->
                    <div class="carousel-item active slide1" style="background-image: url('images/header-carousel/bif.jpg');background-size:100% 100%" alt="bif17"></div>
                    {{--  <!-- Slide Two -->  --}}
                    {{--  <div class="carousel-item slide2" style="background-image: url('images/header-carousel/image1.jpg');background-size:100% 100%" alt="tif16"></div>  --}}
                </div> <!--- End Carousel Inner -->
                {{--  <!--- Previous & Next Buttons -->
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>  --}}
            </div>
        </section>

        <section class="highlight">
            <div class="container">
                <div class="countdown_wrap">

                    <h1 class="countdown_title text-center">BIF XVII <br/> WILL START IN</h1>
                    <div class="heading-underline"></div>

                    <ul id="countdown" data-event-date="16 Juli 2022 09:00:00">
                        <li class="wow zoomIn" data-wow-delay="0s"> <span class="days">00</span>
                            <p class="timeRefDays">days</p>
                        </li>
                        <li class="wow zoomIn" data-wow-delay="0.2s"> <span class="hours">00</span>
                            <p class="timeRefHours">hours</p>
                        </li>
                        <li class="wow zoomIn" data-wow-delay="0.4s"> <span class="minutes">00</span>
                            <p class="timeRefMinutes">minutes</p>
                        </li>
                        <li class="wow zoomIn" data-wow-delay="0.6s"> <span class="seconds">00</span>
                            <p class="timeRefSeconds">seconds</p>
                        </li>
                    </ul>

                </div>
            </div>
        </section>

        <section class="gallery" id="gallery">
            <div class="container offset">
                <div class="title wow fadeInUp">
                    <h1>Gallery</h1>
                    <div class="heading-underline"></div>
                </div>

                <section class="timeline">
                    <div class="timeline-block">
                        <div class="timeline-bullet wow zoomIn" data-wow-delay="0s"></div>
                        <div class="timeline-content">
                            <div class="popup-gallery">
                                <div class="row">
                                  <div class="col">
                                    <a href="{{ asset('images/documentation/2021/gallery_1.jpg') }}" title=""
                                      ><img
                                        src="{{ asset('images/documentation/2021/gallery_1.jpg') }}"
                                        alt="gallery image"
                                        class="img-fluid rounded mx-auto wow fadeIn"
                                    /></a>
                                  </div>

                                  <div class="col-md-6 my-2">
                                    <div class="row justify-content-between">
                                      <div class="col-6">
                                        <a href="{{ asset('images/documentation/2021/gallery_2.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2021/gallery_2.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto wow fadeIn"
                                            data-wow-delay="0.2s"
                                        /></a>
                                      </div>

                                      <div class="col-6">
                                        <a href="{{ asset('images/documentation/2021/gallery_3.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2021/gallery_3.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto wow fadeIn"
                                            data-wow-delay="0.2s"
                                        /></a>
                                      </div>
                                    </div>
                                    <!-- end .row -->

                                    <div class="row">
                                      <div class="col mt-2">
                                        <a href="{{ asset('images/documentation/2021/gallery_4.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2021/gallery_4.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto  wow fadeIn"
                                            data-wow-delay="0.4s"
                                        /></a>
                                      </div>
                                    </div>
                                    <!-- end .row -->
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="timeline-block">
                        <div class="timeline-bullet wow zoomIn" data-wow-delay="0s"></div>
                        <div class="timeline-content">
                            <div class="popup-gallery">
                                <div class="row">
                                  <div class="col">
                                    <a href="{{ asset('images/documentation/2020/gallery_1.jpg') }}" title=""
                                      ><img src="{{ asset('images/documentation/2020/gallery_1.jpg') }}" alt="gallery image"
                                        class="img-fluid rounded mx-auto wow fadeIn"
                                    /></a>
                                  </div>

                                  <div class="col-md-6 my-2">
                                    <div class="row justify-content-between">
                                      <div class="col-6">
                                        <a href="{{ asset('images/documentation/2020/gallery_2.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2020/gallery_2.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto wow fadeIn"
                                            data-wow-delay="0.2s"
                                        /></a>
                                      </div>

                                      <div class="col-6">
                                        <a href="{{ asset('images/documentation/2020/gallery_3.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2020/gallery_3.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto wow fadeIn"
                                            data-wow-delay="0.2s"
                                        /></a>
                                      </div>
                                    </div>
                                    <!-- end .row -->

                                    <div class="row">
                                      <div class="col mt-2">
                                        <a href="{{ asset('images/documentation/2020/gallery_4.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2020/gallery_4.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto  wow fadeIn"
                                            data-wow-delay="0.4s"
                                        /></a>
                                      </div>
                                    </div>
                                    <!-- end .row -->
                                  </div>
                                </div>
                            </div>
                            <span class="date wow flipInX" data-wow-delay="0.3s" style="color: black">2021</span>
                        </div>
                    </div>

                    <div class="timeline-block">
                        <div class="timeline-bullet wow zoomIn" data-wow-delay="0s"></div>
                        <div class="timeline-content">
                            <div class="popup-gallery">
                                <div class="row">
                                  <div class="col">
                                    <a href="{{ asset('images/documentation/2019/gallery_1.jpg') }}" title=""
                                      ><img
                                        src="{{ asset('images/documentation/2019/gallery_1.jpg') }}"
                                        alt="gallery image"
                                        class="img-fluid rounded mx-auto wow fadeIn"
                                    /></a>
                                  </div>

                                  <div class="col-md-6 my-2">
                                    <div class="row justify-content-between">
                                      <div class="col-6">
                                        <a href="{{ asset('images/documentation/2019/gallery_2.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2019/gallery_2.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto wow fadeIn"
                                            data-wow-delay="0.2s"
                                        /></a>
                                      </div>

                                      <div class="col-6">
                                        <a href="{{ asset('images/documentation/2019/gallery_3.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2019/gallery_3.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto wow fadeIn"
                                            data-wow-delay="0.2s"
                                        /></a>
                                      </div>
                                    </div>
                                    <!-- end .row -->

                                    <div class="row">
                                      <div class="col mt-2">
                                        <a href="{{ asset('images/documentation/2019/gallery_4.jpg') }}" title=""
                                          ><img
                                            src="{{ asset('images/documentation/2019/gallery_4.jpg') }}"
                                            alt="gallery image"
                                            class="img-fluid rounded mx-auto  wow fadeIn"
                                            data-wow-delay="0.4s"
                                        /></a>
                                      </div>
                                    </div>
                                    <!-- end .row -->
                                  </div>
                                </div>
                            </div>
                            <span class="date wow flipInX" data-wow-delay="0.3s" style="color: black">2020</span>
                        </div>
                    </div>
                </section>
            </div>
        </section>

        <section class="subco" id="sponsor">
            <div class="container offset">
                <div class="title wow fadeInUp">
                    <h1>Our Sponsors</h1>
                    <div class="heading-underline"></div>
                </div>

                <div class="row justify-content-center">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm">
                                <center> <img src="{{ asset('images/top/1.png') }}" alt="top" width="400"/> </center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/top/2.png') }}" alt="top" width="300"/></center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/top/3.png') }}" alt="top" width="200"/></center>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-sm">
                                <center> <img src="{{ asset('images/Gold/1.png') }}" alt="gold" width="300"/> </center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Gold/4.png') }}" alt="gold" width="300"/></center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Gold/5.png') }}" alt="gold" width="200"/></center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm">
                                <center> <img src="{{ asset('images/Gold/2.png') }}" alt="gold" width="350"/> </center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Gold/3.png') }}" alt="gold" width="300"/></center>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-sm">
                                <center> <img src="{{ asset('images/Silver/1.png') }}" alt="gold" width="150"/> </center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Silver/2.png') }}" alt="gold" width="150"/></center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Silver/3.png') }}" alt="gold" width="150"/></center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Silver/4.png') }}" alt="gold" width="200"/></center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <center> <img src="{{ asset('images/Silver/5.png') }}" alt="gold" width="150"/> </center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Silver/6.png') }}" alt="gold" width="150"/></center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Silver/7.png') }}" alt="gold" width="150"/></center>
                            </div>
                            <div class="col-sm">
                                <center><img src="{{ asset('images/Silver/8.png') }}" alt="gold" width="100"/></center>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <section class="footer" id="footer">
            <footer>

                <div class="social-icons">
                    <a href="https://tinyurl.com/fbbinabusana" target="blank" class="wow zoomIn"> <i class="fab fa-facebook-f"></i> </a>
                    <a href="http://instagram.com/binabusana.id" target="blank" class="wow zoomIn" data-wow-delay="0.2s"> <i class="fab fa-instagram"></i></a>
                    <a href="https://www.linkedin.com/company/binabusana-internusa-group" target="blank" class="wow zoomIn" data-wow-delay="0.2s"> <i class="fab fa-linkedin-in"></i></a>
                </div>
                <p> <small class="text-muted">Copyright © 2022. All rights reserved. Made in Semarang</small></p>

            </footer>
        </section>


        <div class="modal fade" id="registerModal" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header bg-tif">
                <h5 class="modal-title" id="registerModalLabel">Register</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form class="wizard-form steps-basic" method="POST" action="{{ route('registration') }}"
                enctype= "multipart/form-data"
                id="form_regis" data-fouc>
                  <h6>Personal data</h6>
                  <fieldset>
                    <div class="form-group row">
                      <label class="font-weight-bold col-lg-3" for="exampleFormControlInput1">NIK</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" name="nik" id="nik" placeholder="Input NIK" value="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="font-weight-bold col-lg-3" for="exampleFormControlInput1">Password</label>
                      <div class="col-lg-9">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Input Your Password (DDMMYYYY)" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="font-weight-bold col-lg-3" for="exampleFormControlInput1">Phone Number</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" name="phonenumber" id="phonenumber" placeholder="Input Your Phone Number" autocomplete="off">
                      </div>
                    </div>
                    <input id="check_nik" type="hidden" value="0">
                    <input id="message" type="hidden">
                    <input id="invitation" name="invitation" type="hidden">
                  </fieldset>

                  <h6>Vote</h6>
                  <fieldset>
                    <div class="row">
                      @foreach ($tiktokVoters as $igVoter)
                        <div class="col-lg-6 col-sm-12 col-sm-12 my-1">
                          <div class="card card-ig" id="idIG{{ $igVoter->id }}">
                            <div class="embed-responsive embed-responsive-4by3">
                              <video  controls disablepictureinpicture controlsList="nodownload noremoteplayback noplaybackrate" class="embed-responsive-item video-full">
                                <source src="{{ $igVoter->voter_url }}" type="video/mp4" />
                              </video>
                           </div>
                            <div class="card-body">
                              <i class="fas fa-heart mr-2" style="color:red"></i> <span class="total-vote" id="totalIg{{ $igVoter->id }}">???</span>
                              <button type="button" class="btn btn-primary rounded-pill btn-sm btn-block my-1 btn-vote-ig" data-id="{{ $igVoter->id }}">Vote Me </button>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    </div>

                  </fieldset>

                  <h6>Confirm My Seat</h6>
                  <fieldset>
                    <div class="form-group row">
                      <label class="font-weight-bold col-lg-3" for="exampleFormControlInput1">NIK</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" id="nik-label" readonly>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="font-weight-bold col-lg-3" for="exampleFormControlInput1">Nama</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" id="name-label"readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="font-weight-bold col-lg-3" for="exampleFormControlInput1">Position</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" id="position-label" name="position" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="font-weight-bold col-lg-3" for="exampleFormControlInput1">Unit</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" id="unit-label" name="unit" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="font-weight-bold col-lg-3" for="exampleFormControlInput1">No. Hp</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" id="no-hp-label" readonly>
                      </div>
                    </div>
                  </div>
                  </fieldset>

                  <input id="voted_tiktok" name="voted_tiktok" type="hidden" value="">
                  <input id="voted_ig" name="voted_ig" type="hidden" value="">

                </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <button type="button" class="d-none" id="alert_warning"></button>
        <button type="button" class="d-none" id="alert_success"></button>

        <script src="{{ mix('js/plugins.js') }}"></script>
        <script>

        </script>
    </body>
</html>
