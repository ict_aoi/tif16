<?php

return [
    'invitation'    => storage_path() . '/app/invitation',
    'voters'        => storage_path() . '/app/voters',
];
